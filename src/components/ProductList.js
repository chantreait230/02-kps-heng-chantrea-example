import React from 'react'
import { Table,Button } from 'react-bootstrap';

export default function ProductList(props) {
    let myproducts=props.products.map((p)=> 
        <tr key={p.id} className={p.isHiding ? "bg-success":null}>
            <td>{p.id}</td>
            <td>{p.name}</td>
            <td>{p.importDate}</td>
            <td>
                <Button 
                    variant={p.isHiding ? "primary":"danger"} 
                    onClick={()=> props.onHide(p.id-1)}>{p.isHiding ? "UnHide":"Hide"}
                </Button>
            </td>
        </tr>
    )
    return (
        <div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Import Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {myproducts}
                </tbody>
            </Table>
        </div>
    )
}
