import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductList from './components/ProductList'

export default class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      products: [
        {
        id: 1,
        name: "Carabao",
        importDate: "09-03-2020",
        isHiding: false,
     },
       {
        id: 2,
      name: "Pepsi",
        importDate: "10-01-2020",
        isHiding: false,
       },
       {
        id: 3,
      name: "Samurai",
        importDate: "12-03-2020",
        isHiding: false,
       },
       {
        id: 4,
      name: "Coca Cola",
        importDate: "12-04-2020",
        isHiding: false,
       },
       {
        id: 5,
      name: "Bacchus",
        importDate: "12-04-2019",
        isHiding: false,
       },
     ],

    }
  }
  onHide=(id)=>{
    let products= [...this.state.products]
    products[id].isHiding=!products[id].isHiding
    this.setState({
      products
    })
  }
  render() {
    return (
      <div>
        <ProductList products={this.state.products} onHide={this.onHide}/>
      </div>
    )
  }
}

